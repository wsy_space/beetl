package org.beetl.android.internal.util;

public class EmptyArray {
    public static final int[] INT = new int[]{};
    public static final Object[] OBJECT = new Object[]{};
    public static final String[] STRING = new String[]{};
    public static final boolean[] BOOLEAN = new boolean[]{};
    public static final long[] LONG = new long[]{};
}
