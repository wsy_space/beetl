package org.beetl.core.om.asm;

/**
 * 类描述
 *
 * @author laozhaishaozuo@foxmail.com
 */
public interface TypeDescriptorConstants {
    String INT_ = "I";
    String VOID_ = "V";
    String BOOLEAN_ = "Z";
    String BYTE_ = "B";
    String CHARACTER_ = "C";
    String SHORT_ = "S";
    String DOUBLE_ = "D";
    String FLOAT_ = "F";
    String LONG_ = "J";
}
